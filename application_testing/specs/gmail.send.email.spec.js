var GmailLoginPage = require("../../application_testing/page_objects/gmail.login.page.js");
var GmailMainPage = require("../../application_testing/page_objects/gmail.main.page.js");

browser.ignoreSynchronization = true;

describe("Testing an email sending function of gmail:", function() {
  let gmailLoginPage;
  let gmailMainPage;
  let EC = protractor.ExpectedConditions;

  beforeEach(function() {
    gmailLoginPage = new GmailLoginPage(browser.get("https://gmail.com"));
    gmailMainPage = new GmailMainPage();
  });

  it("1) send a regular email", function() {
    gmailLoginPage.loginToGmail("testnadira@gmail.com", "123Password");

    browser.wait(EC.elementToBeClickable(gmailMainPage.emailComposeButton));

    gmailMainPage.composeNewEmail(
      "testnadira2@gmail.com",
      "New Subject",
      "Hello, world"
    );

    gmailMainPage.sentMailCategoryButton.click();
    browser.sleep(1000);
  });
});
