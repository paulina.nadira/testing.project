class GmailLoginPage {
  constructor(url) {
    this.emailInput = element(by.id("identifierId"));
    this.emailNextButton = element(by.id("identifierNext"));
    this.passwordInput = element(by.name("password"));
    this.passwordNextButton = element(by.id("passwordNext"));
  }

  loginToGmail(username, password) {
    this.emailInput.sendKeys(username);
    this.emailNextButton.click();
    browser.sleep(1000);
    this.passwordInput.sendKeys(password);
    this.passwordNextButton.click();
  }
}

module.exports = GmailLoginPage;
