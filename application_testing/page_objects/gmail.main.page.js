let EC = protractor.ExpectedConditions;

class GmailMainPage {
  constructor() {
    this.emailComposeButton = element(by.className("T-I J-J5-Ji T-I-KE L3"));
    this.newMessageToInput = element(by.name("to"));
    this.newMessageSubjectInput = element(by.name("subjectbox"));
    this.newMessageBodyInput = element(by.className("Am Al editable LW-avf"));
    this.newMessageSendButton = element(
      by.className("T-I J-J5-Ji aoO T-I-atl L3")
    );
    this.sentMailCategoryButton = element(
      by.css('a[href*="https://mail.google.com/mail/u/0/#sent"]')
    );
  }

  composeNewEmail(recipientEmail, subject, body) {
    this.emailComposeButton.click();
    browser.wait(EC.visibilityOf(this.newMessageToInput));
    this.newMessageToInput.sendKeys(recipientEmail);
    this.newMessageSubjectInput.sendKeys(subject);
    this.newMessageBodyInput.sendKeys(body);
    this.newMessageSendButton.click();
    browser.sleep(2000);
  }
}

module.exports = GmailMainPage;
