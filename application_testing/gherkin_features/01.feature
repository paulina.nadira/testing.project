Feature: Login to gmail.com

  Registered gmail user should be able to login to their gmail account and view their emails.
  This is required to keep users content with provided email services.

  Rules:
  - user must have an active google account
  - user must provide valid username: "testnadira@gmail.com"
  - user must provide valid password: "123Password"

  Background:
    Given user has opened the www.gmail.com page
      And is redirected to login page

  Scenario: user tries to login with wrong username
    Given user has input wrong username "coś" in the "Email or phone" field
    When they click on button "Next"
    Then alert "Enter a valid email or phone number" is displayed

  Scenario: user tries to login with wrong password
    Given user has input valid username "testnadira@gmail.com" in the "Email or phone" field
      And they click on button "Next"
    When they input wrong password "123" in the "Enter your password" field
      And they click on button "Next"
    Then alert "Wrong password. Try again or click Forgot password to reset it." is displayed

  Scenario: user tries to login with valid credentials
    Given user has input valid username "testnadira@gmail.com" in the "Email or phone" field
      And they click on button "Next"
    When they input valid password "123Password" in the "Enter your password" field
      And they click on button "Next"
    Then user is redirected to their gmail home page
      And their emails list is displayed
