Feature: Send email

  Logged gmail user should be able to send an email from their account.
  This is required so that users can communicate with their contacts.

  Background:
    Given user has logged in to gmail as "testnadira@gmail.com" using password "123Password"
      And user has clicked "Compose" button
      And "New Message" window has appeared

  Scenario: user sends an email with attachment
    Given user has input contact email "testnadira@gmail.com" in "To" field
      And they have input "My subject" in "Subject" field
      And they have input "Hello, world" in message field
      And they clicked on "Attach files" to attach a file
      And selected a file to attach
    When they click on "Send" button
    Then "New Message" window will disappear
      And new email will appear in Inbox
      And new email will have attachment icon

  Scenario: user sends an empty email
    Given user has input contact email "testnadira@gmail.com" in "To" field
    When they click on "Send" button
      And click "OK" on "Send this message without a subject or text in the body?" pop-up
    Then "New Message" window will disappear
      And new email with "(no subject)" will appear in Inbox

  Scenario: user checks Sent Mail
    Given user has sent at least one email
    When user clicks on "Sent Mail"
      And is redirected to "Sent Mail" page
    Then user will see all sent emails as a list