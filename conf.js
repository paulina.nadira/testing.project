exports.config = {
  seleniumAddress: "http://localhost:4444/wd/hub",

  capabilities: {
    browserName: "chrome"
  },

  framework: "jasmine",

  specs: ["application_testing/specs/gmail.send.email.spec.js"],

  suites: {
    gmail: "application_testing/specs/gmail.send.email.spec.js"
  }
};
